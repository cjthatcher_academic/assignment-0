package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		
		/*
		 * Instantiate an instance of Student that includes your own personal information
		 * Feel free to make up information if you would like.
		 */
		
		
		System.out.println(student);
		
		/*
		 * Print out the student information. 
		 */
	}

}