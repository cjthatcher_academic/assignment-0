package edu.usu.cs.oo;

public class Job {

	private String jobTitle;
	private int basePay;
	private Company company;
	
	public Job(String jobTitle, int basePay, Company company)
	{
		this.jobTitle = jobTitle;
		this.basePay = basePay;
		this.company = company;
	}
	
	/*
	 * Generate the getters and setters here. Yes. I know.
	 * Generating getters and setters is boring.
	 * Right click -> Source -> Generate Getters / Setters -> Follow the instructions.
	 * You're welcome.
	 */
	
	
	@Override
	public String toString()
	{
		return "Job: " + company + ", " + jobTitle + " for " + basePay + " dollars per year";
	}
}
